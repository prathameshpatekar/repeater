package be.kdg.java2.repeater;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RepeaterApplication {

    public static void main(String[] args) {
        SpringApplication.run(RepeaterApplication.class, args);
    }

}
